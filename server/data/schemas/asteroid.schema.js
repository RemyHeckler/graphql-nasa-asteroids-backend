export const asteroid = `
  type Asteroid {
    id: String
    name: String
    minDiametr: Float
    maxDiametr: Float
  }
`;
