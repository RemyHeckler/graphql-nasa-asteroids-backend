export const query = `
  type Query {
    days (startDate: String, endDate: String): [Day]
    asteroid (id: String): Asteroid
  }
`;
