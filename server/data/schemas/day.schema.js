export const day = `
  type Day {
    day: String
    asteroids: [Asteroid] 
  }
`;
