import { makeExecutableSchema } from 'graphql-tools';

import { getDetails, getDays } from './actions';
import { asteroid, day, query } from './schemas';

const typeDefs = `
  ${asteroid}
  ${day}
  ${query}
`;

const resolvers = {
  Query: {
    days: getDays,
    asteroid: getDetails,
  },
};

export const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});
