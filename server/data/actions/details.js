import fetch from 'node-fetch';

export async function getDetails(obj, args) {
  try {
    const asteroid = await fetch(`${process.env.DETAILSURL}${args.id}?api_key=${process.env.KEY}`)
      .then(res => res.json());
    return {
      name: asteroid.name,
      minDiametr: asteroid.estimated_diameter.meters.estimated_diameter_min,
      maxDiametr: asteroid.estimated_diameter.meters.estimated_diameter_max,
    };
  } catch (e) {
    return new Error('server error');
  }
}
