import fetch from 'node-fetch';

import { rangeValidation } from '../../utils';

export async function getDays(obj, args) {
  const { startDate, endDate } = args;
  rangeValidation(startDate, endDate);
  try {
    const list = await fetch(`${process.env.LISTURL}start_date=${startDate}&end_date=${endDate}&api_key=${process.env.KEY}`)
      .then(res => res.json());
    const asteroids = list.near_earth_objects;
    const days = Object.keys(asteroids);
    const asteroidsList = days.map(item => ({
      day: item,
      asteroids: asteroids[item].map(point => ({ name: point.name, id: point.neo_reference_id })),
    }));
    return asteroidsList;
  } catch (e) {
    return new Error('server error');
  }
}
