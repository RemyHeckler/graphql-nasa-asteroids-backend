export const rangeValidation = (startDate, endDate) => {
  const now = Date.now();
  const start = Date.parse(startDate);
  const end = Date.parse(endDate);

  if (now < end) {
    throw new Error('range error');
  }
  if (start > end) {
    throw new Error('range error');
  }
  if (end - start > 604800000) {
    throw new Error('range error');
  }
  return null;
};
